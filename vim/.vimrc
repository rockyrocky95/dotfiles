
colorscheme retrobox
syntax on
filetype plugin indent on
filetype on 


" SETTINGS  ------------------------------------------------------------------

set background=dark
set termguicolors
set t_Co=256
set clipboard=unnamed
set nocompatible
set laststatus=2
set backspace=indent,eol,start
set number
set relativenumber
set scrolloff=8
set sidescroll=8
set ruler
set showcmd
set nowrap
set nobackup
set incsearch
set ignorecase
set smartcase
set showmode
set showmatch
set hlsearch
set history=1000

" CURSOR SHAPE ---------------------------------------------------------------

let &t_SI = "\e[5 q"   " cursor in insert mode
let &t_EI = "\e[2 q"   " cursor in normal mode
let &t_SR = "\e[3 q"   " cursor in replace mode
let &t_ti .= "\e[2 q"  " cursor when vim starts
let &t_te .= "\e[1 q"  " cursor when vim exits


" CURSOR HIGHLIGHT -----------------------------------------------------------

function s:SetCursorLine()
	set cursorline
	hi cursorline cterm=none ctermbg=lightgrey ctermfg=black
endfunction

autocmd VimEnter * call s:SetCursorLine()

