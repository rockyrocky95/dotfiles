#    ▄▄▄▄▄▄▄ ▄▄▄ ▄▄▄▄▄▄▄ ▄▄   ▄▄    ▄▄▄▄▄▄▄ ▄▄   ▄▄ ▄▄▄▄▄▄▄ ▄▄▄     ▄▄▄     
#   █       █   █       █  █ █  █  █       █  █ █  █       █   █   █   █    
#   █    ▄▄▄█   █  ▄▄▄▄▄█  █▄█  █  █  ▄▄▄▄▄█  █▄█  █    ▄▄▄█   █   █   █    
#   █   █▄▄▄█   █ █▄▄▄▄▄█       █  █ █▄▄▄▄▄█       █   █▄▄▄█   █   █   █    
#   █    ▄▄▄█   █▄▄▄▄▄  █   ▄   █  █▄▄▄▄▄  █   ▄   █    ▄▄▄█   █▄▄▄█   █▄▄▄ 
#   █   █   █   █▄▄▄▄▄█ █  █ █  █   ▄▄▄▄▄█ █  █ █  █   █▄▄▄█       █       █
#   █▄▄▄█   █▄▄▄█▄▄▄▄▄▄▄█▄▄█ █▄▄█  █▄▄▄▄▄▄▄█▄▄█ █▄▄█▄▄▄▄▄▄▄█▄▄▄▄▄▄▄█▄▄▄▄▄▄▄█

#======================================================================================================#
# 01000111 01101111 01100100  01101100 01101111 01110110 01100101 01110011  01111001 01101111 01110101 #
#======================================================================================================#

# -*- set -*-

set EDITOR "nvim"                        # $EDITOR use neovim in terminal
set -g fish_greeting ""                  # supress greetings
set -U fish_history_duplicate_duration 0 # ignores duplicates 

# -*- fish syntax highlight -*-

set fish_color_command green
set fish_color_quote magenta --italics
set fish_color_param blue
set fish_color_autosuggestion white --italics
set fish_color_valid_path --italics
set fish_color_option yellow
set fish_color_operator blue
set fish_color_error red 
set fish_color_end magenta

# -*- fish prompt -*-

function fish_prompt
    echo
    set_color green
    echo -n (whoami)""
    set_color yellow
    echo -n "@"
    set_color blue
    echo -n (hostname) ""
    set_color magenta
    echo -n (prompt_pwd --full-length-dirs 5)""
    set_color red
    echo -n (fish_git_prompt)""
    set_color normal
    echo 
    echo -n "→ "
end

# -*- abbreviations -*-

# navigation
abbr ls    "eza -a"
abbr ll    "eza -al --time-style '+%d %b %y'"
abbr tree  "eza -a --tree"
abbr cd-   "cd -"
abbr cd.   "cd .."
abbr cd..  "cd ../.."
abbr cd... "cd ../../.."

# terminal session
abbr cc  "cd && clear"
abbr c   "clear"
abbr x   "exit"
abbr :wq "exit"

# directories/files cammands
abbr mkdir "mkdir -v"
abbr rmr   "rm -vr -I"
abbr rm    "rm -v"
abbr cp    "cp -v"
abbr mv    "mv -v"

# terminal commands
abbr font-cache   "fc-cache -f -v" 
abbr font-list    "fc-list : family | sort" 
abbr font-search  "fc-list : family | sort | grep -i"
abbr fish-reload  "source ~/.config/fish/config.fish"
abbr find         "sudo find / -iname"

# programs
abbr stow   "stow -v"
abbr stowd  "stow -vD"
abbr vim    "nvim"
abbr cat    "bat --theme='base16'"
abbr matrix "cmatrix | lolcat -x"
abbr ff     "fastfetch --config ~/.config/fastfetch/smallfetch.jsonc"
abbr fetch  "fastfetch --config /usr/share/fastfetch/presets/neofetch.jsonc"

# system sessions options
abbr shutdown "systemctl poweroff"
abbr reboot   "systemctl reboot"
abbr sleep    "systemctl suspend"

# dnf
abbr dnfup "sudo dnf update"
abbr dnfin "sudo dnf install"
abbr dnfrm "sudo dnf remove"
abbr dnfcu "dnf check-update"
abbr dnfse "dnf search"

# flatpak
abbr flatup "flatpak update"
abbr flatin "flatpak install"
abbr flatrm "flatpak uninstall"
abbr flatun "flatpak uninstall --unused"
abbr flatse "flatpak search"

# git
abbr gs "git status"
abbr ga "git add"
abbr gc "git commit -m"
abbr gp "git push"

#  -*- functions -*-

# launching fastfetch's configs for different terminals
if test "$TERM" = "xterm-kitty"
    fastfetch --config ~/.config/fastfetch/kitty.jsonc
else if test "$TERM" = "xterm-256color"
    fastfetch --config ~/.config/fastfetch/smallfetch.jsonc
end

# function for extracting files 
function ex -a file
    if test -f "$file"
        switch "$file"
            case "*.tar.xz"
                tar xf $file
            case "*.tar.bz2"
                tar xjf $file
            case "*.tar.gz"
                tar xzf $file
            case "*.bz2"
                bunzip2 $file
            case "*.rar"
                unrar x $file
            case "*.gz"
                gunzip $file
            case "*.tar"
                tar xf $file
            case "*.tbz2"
                tar xjf $file
            case "*.tgz"
                tar xzf $file
            case "*.zip"
                unzip $file
            case "*.Z"
                uncompress $file
            case "*.7z"
                7z x $file
            case "*"
                echo "'$file' cannot be extracted via ex()"
        end
    else
        echo "'$file' is not a valid file"
    end
end

# functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

# the bindings for !! and !$
if [ "$fish_key_bindings" = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

