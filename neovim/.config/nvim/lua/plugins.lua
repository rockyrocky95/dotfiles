local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)

    use 'wbthomason/packer.nvim'              -- packer
    use 'norcalli/nvim-colorizer.lua'         -- colors
    use 'nvim-treesitter/nvim-treesitter'     -- syntax highlighting
    use 'nvim-lualine/lualine.nvim'           -- status line 
    use 'Mofiqul/dracula.nvim'                -- theme

  if packer_bootstrap then
    require('packer').sync()
  end
end)
