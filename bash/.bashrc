#   ▄▄▄▄▄▄▄ ▄▄▄▄▄▄ ▄▄▄▄▄▄▄ ▄▄   ▄▄    ▄▄▄▄▄▄▄ ▄▄   ▄▄ ▄▄▄▄▄▄▄ ▄▄▄     ▄▄▄     
#  █  ▄    █      █       █  █ █  █  █       █  █ █  █       █   █   █   █    
#  █ █▄█   █  ▄   █  ▄▄▄▄▄█  █▄█  █  █  ▄▄▄▄▄█  █▄█  █    ▄▄▄█   █   █   █    
#  █       █ █▄█  █ █▄▄▄▄▄█       █  █ █▄▄▄▄▄█       █   █▄▄▄█   █   █   █    
#  █  ▄   ██      █▄▄▄▄▄  █   ▄   █  █▄▄▄▄▄  █   ▄   █    ▄▄▄█   █▄▄▄█   █▄▄▄ 
#  █ █▄█   █  ▄   █▄▄▄▄▄█ █  █ █  █   ▄▄▄▄▄█ █  █ █  █   █▄▄▄█       █       █
#  █▄▄▄▄▄▄▄█▄█ █▄▄█▄▄▄▄▄▄▄█▄▄█ █▄▄█  █▄▄▄▄▄▄▄█▄▄█ █▄▄█▄▄▄▄▄▄▄█▄▄▄▄▄▄▄█▄▄▄▄▄▄▄█
 
#======================================================================================================#
# 01000111 01101111 01100100  01101100 01101111 01110110 01100101 01110011  01111001 01101111 01110101 #
#======================================================================================================#

# export
export HISTCONTROL=ignoredups:erasedups # no duplicate entries.
export EDITOR='nvim'                    # $EDITOR use neovim in terminal.
export VISUAL='nvim'                    # $VISUAL use neovim in GUI mode.

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
	PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# tabbing will ignore capitalization
bind 'set completion-ignore-case on'

# ░█▀▄░█▀█░█▀▀░█░█░░░█▀█░█▀▄░█▀█░█▄█░█▀█░▀█▀
# ░█▀▄░█▀█░▀▀█░█▀█░░░█▀▀░█▀▄░█░█░█░█░█▀▀░░█░
# ░▀▀░░▀░▀░▀▀▀░▀░▀░░░▀░░░▀░▀░▀▀▀░▀░▀░▀░░░░▀░

# foreground
red="\[$(tput setaf 1)\]"
green="\[$(tput setaf 2)\]"
yellow="\[$(tput setaf 3)\]"
blue="\[$(tput setaf 4)\]"
magenta="\[$(tput setaf 5)\]"
cyan="\[$(tput setaf 6)\]"
white="\[$(tput setaf 7)\]"

# other
bold="\[$(tput bold)\]"
reset="\[$(tput sgr0)\]"

# git and git status
function parse_git_dirty {
  [[ $(git status --porcelain 2> /dev/null) ]] && echo "*"
}

function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/ (\1$(parse_git_dirty))/"
}

# BASH prompt
export PS1="\n${magenta}\u ${reset}on ${blue}\h ${reset}in ${green}\w${red}\$(parse_git_branch)\n${reset}↪ "

# ░█▀█░█░░░▀█▀░█▀█░█▀▀░█▀▀░█▀▀
# ░█▀█░█░░░░█░░█▀█░▀▀█░█▀▀░▀▀█
# ░▀░▀░▀▀▀░▀▀▀░▀░▀░▀▀▀░▀▀▀░▀▀▀

# navigation
alias ls="eza -aF" 
alias ll="eza -alF --time-style '+%d %b %y'"
alias tree="eza --tree -a"
alias cd.="cd .."
alias cd..="cd ../.."
alias cd...="cd ../../.."

# terminal session
alias cc="cd && clear"
alias c="clear"
alias x="exit"
alias :wq="exit"

# directories/files command aliases 
alias mkdir="mkdir -v"
alias rmr="rm -vr -I"
alias rm="rm -v"
alias cp="cp -v"
alias mv="mv -v"

# terminal commands
alias font-cache="fc-cache -f -v"
alias font-list="fc-list : family | sort"
alias font-search="fc-list : family | sort | grep -i"
alias find="sudo find / -iname"

# program aliases
alias stow="stow -v"
alias stowd="stow -vD"
alias vim="nvim"
alias cat="bat --theme='base16'"
alias matrix="cmatrix | lolcat -x"
alias ff="fastfetch --config ~/.config/fastfetch/smallfetch.jsonc"
alias fetch="fastfetch --config /usr/share/fastfetch/presets/neofetch.jsonc"

# system sessions options
alias shutdown="systemctl poweroff"
alias reboot="systemctl reboot"
alias sleep="systemctl suspend"

# dnf aliases
alias dnfup="sudo dnf update"
alias dnfin="sudo dnf install"
alias dnfrm="sudo dnf remove"
alias dnfcu="dnf check-update"
alias dnfse="dnf search"

# flatpak aliases
alias flatup="flatpak update"
alias flatin="flatpak install"
alias flatrm="flatpak uninstall"
alias flatun="flatpak uninstall --unused"
alias flatse="flatpak search"

# git aliases
alias gs="git status"
alias ga="git add"
alias gc="git commit -m"
alias gp="git push"

# ░█▀▀░█░█░█▀█░█▀▀░▀█▀░▀█▀░█▀█░█▀█░█▀▀
# ░█▀▀░█░█░█░█░█░░░░█░░░█░░█░█░█░█░▀▀█
# ░▀░░░▀▀▀░▀░▀░▀▀▀░░▀░░▀▀▀░▀▀▀░▀░▀░▀▀▀

# launching fastfetch's configs for different terminals 
if [[ "$TERM" == "xterm-kitty" ]]; then
    fastfetch --config ~/.config/fastfetch/kitty.jsonc
elif [[ "$TERM" == "xterm-256color" ]]; then
    fastfetch --config ~/.config/fastfetch/smallfetch.jsonc
fi

# usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

