#!/bin/bash

# =============================== #
# fedora post installation script #
# =============================== #

# === dnf.conf ===

sudo rm -v /etc/dnf/dnf.conf
sudo cp  -v ~/dotfiles/scripts/fedora/assets/dnf.conf /etc/dnf/

# === changing host name ===

sudo hostnamectl set-hostname "soda"

# === updating fedora ===

sudo dnf update -y 

# === fedora essentials ===

# RPM Fusion (both free and non-free)
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm 
sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# VLC video player
sudo dnf install -y vlc

# media codecs
sudo dnf swap ffmpeg-free ffmpeg --allowerasing --assumeyes
sudo dnf update -y @multimedia --setopt="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin

# === applications ===

# application for the GNOME desktop environmnet 
desktop_session=$(echo $DESKTOP_SESSION)

# Check if the desktop session is GNOME
if [ "$desktop_session" == "gnome" ]; then
    echo "GNOME desktop session found! Running GNOME-specific command."
    sudo dnf install -y gnome-tweaks gnome-extensions-app
else
    echo "This is not a GNOME session. No action taken."
fi

# installing flatpak if not installed
sudo dnf install -y flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# installing brave-browser 
sudo dnf install -y dnf-plugins-core
sudo dnf config-manager addrepo --from-repofile=https://brave-browser-rpm-release.s3.brave.com/brave-browser.repo
sudo dnf install -y brave-browser

# installing librewolf browser 
curl -fsSL https://repo.librewolf.net/librewolf.repo | pkexec tee /etc/yum.repos.d/librewolf.repo
sudo dnf install -y librewolf

# installing applications from repository
sudo dnf install -y alacritty kitty neovim fish stow fastfetch eza bat cmatrix lolcat btop gparted

# removing applications
sudo dnf remove -y nano dnfdragora firefox

# installing NvChad for neovim 
git clone https://github.com/NvChad/starter ~/.config/nvim/

# === setting up dotfiles with GNU Stow ===

mv -v ~/.bashrc ~/.old_bashrc
rm -rv ~/.local/share/backgrounds/
cd ~/dotfiles && stow -v alacritty/ bash/ kitty/ backgrounds/ fastfetch/ fish/ fonts/ 

# === ending message ===

echo " "
echo " ▄▄▄▄▄▄  ▄▄▄▄▄▄▄ ▄▄    ▄ ▄▄▄▄▄▄▄    ▄▄ ▄▄ " 
echo "█      ██       █  █  █ █       █  █  █  █"
echo "█  ▄    █   ▄   █   █▄█ █    ▄▄▄█  █  █  █"
echo "█ █ █   █  █ █  █       █   █▄▄▄   █  █  █"
echo "█ █▄█   █  █▄█  █  ▄    █    ▄▄▄█  █▄▄█▄▄█"
echo "█       █       █ █ █   █   █▄▄▄    ▄▄ ▄▄ "
echo "█▄▄▄▄▄▄██▄▄▄▄▄▄▄█▄█  █▄▄█▄▄▄▄▄▄▄█  █▄▄█▄▄█"

